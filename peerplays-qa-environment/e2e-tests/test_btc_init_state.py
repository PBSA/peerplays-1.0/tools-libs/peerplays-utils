import pytest

from bitcoin_utils import *
from expected_values import *


@pytest.mark.order(1)
def test_wallets():
    assert btc_listwallets() == ["default", "son-wallet", "distribution"]


@pytest.mark.order(1)
def test_addresses():
    for address in BTC_ADDRESSES_AND_KEY:
        assert btc_dumpprivkey(address[0]) == address[1]


@pytest.mark.order(1)
def test_balances():
    for address in BTC_ADDRESSES_AND_KEY:
        transactions = btc_listunspent_per_address(
            address[0], _min_amount=850.0, _max_amount=950.0)
        assert len(transactions) == 1
        assert transactions[0]["amount"] == 910.0
