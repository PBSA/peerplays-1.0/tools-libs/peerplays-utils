#!/usr/bin/python3

import docker
import pexpect
import subprocess
import sys
import time

from string_utils import *

DEBUG = True
STD_OUT = sys.stdout if DEBUG else subprocess.DEVNULL
ERR_OUT = sys.stderr if DEBUG else subprocess.STDOUT

DOCKER_CLIENT = docker.from_env()

PP_DOCKER_IMAGE_NAME_ALL_IN_ONE = 'peerplays-all-in-one'
PP_DOCKER_IMAGE_NAMES_MULTI_NODE = (
    'peerplays01',
    'peerplays02',
    'peerplays03',
    'peerplays04',
    'peerplays05',
    'peerplays06',
    'peerplays07',
    'peerplays08',
    'peerplays09',
    'peerplays10',
    'peerplays11',
    'peerplays12',
    'peerplays13',
    'peerplays14',
    'peerplays15',
    'peerplays16')
BTC_DOCKER_IMAGE_NAME = 'bitcoin-for-peerplays'
ETH_DOCKER_IMAGE_NAME = 'ethereum-for-peerplays'
HIVE_DOCKER_IMAGE_NAME = 'hive-for-peerplays'


def get_docker_container_by_image_name(_image_name):
    for containter in DOCKER_CLIENT.containers.list():
        if _image_name in containter.name:
            return containter
    raise RuntimeError(_image_name + ' container not started')


def stop_all_dockers():
    print_separator_start('Stopping all docker containers')
    process = subprocess.run(['docker-compose',
                              'down'],
                             universal_newlines=True,
                             stdout=STD_OUT,
                             stderr=ERR_OUT)
    print_separator_end('All docker containers stopped')


def start_docker(
        _image_names,
        _message=None,
        timeout_after_last_message=None):

    all_images = ' '.join(_image_names)
    assert len(all_images) > 0

    if _message:
        PP_DOCKER_COMPOSE_UP_TIMEOUT = 300
        pexpect_client = pexpect.spawn('docker-compose up ' + all_images)

        # Wait until the message is printed in docker
        try:
            pexpect_client.expect(
                _message, timeout=PP_DOCKER_COMPOSE_UP_TIMEOUT)
        except pexpect.TIMEOUT:
            print('Timed out while staring container ' + all_images)
            print(pexpect_client.before)
            return -1

        if timeout_after_last_message:
            # Wait until the message is no longer printed
            while True:
                try:
                    pexpect_client.expect_exact(
                        _message, timeout=timeout_after_last_message)
                except pexpect.TIMEOUT:
                    break
    else:
        # Run without waiting for message
        pexpect.run('docker-compose up ' + all_images)
        # Wait in a loop for all images started, because it seems that there is some delay before containers become actually available.
        # At each iteration, check if _all_ image names are substrings inside a
        # name of _any_ of the started containers
        while not all(any(image_name in cont.name for cont in DOCKER_CLIENT.containers.list())
                      for image_name in _image_names):
            time.sleep(1)
        time.sleep(5)

    return 0


def stop_docker(_image_name):
    process = subprocess.run(['docker-compose',
                              'stop', _image_name],
                             universal_newlines=True,
                             stdout=STD_OUT,
                             stderr=ERR_OUT)


def start_btc():
    print_separator_start('Starting Bitcoin docker container')
    start_docker([BTC_DOCKER_IMAGE_NAME], 'msghand thread start')
    btc_docker_container = get_docker_container_by_image_name(
        BTC_DOCKER_IMAGE_NAME)
    print_separator_end(
        'Docker Bitcoin started, docker id is ' +
        btc_docker_container.short_id)
    return btc_docker_container


def init_network_btc(_btc_docker_conatiner):
    print_separator_start('Initializing Bitcoin network')
    process = subprocess.run(['docker',
                              'exec',
                              _btc_docker_conatiner.short_id,
                              'sh',
                              '-c',
                              './init-network.sh'],
                             stdout=STD_OUT,
                             stderr=ERR_OUT,
                             universal_newlines=True)
    print_separator_end('Bitcoin network initialized')


def start_eth():
    print_separator_start(
        'Starting Ethereum docker container')
    start_docker([ETH_DOCKER_IMAGE_NAME], 'Generating DAG in progress', 10)
    eth_docker_conatiner = get_docker_container_by_image_name(
        ETH_DOCKER_IMAGE_NAME)
    print_separator_end(
        'Docker Ethereum started, docker id is ' +
        eth_docker_conatiner.short_id)
    return eth_docker_conatiner


def init_network_eth(_eth_docker_conatiner):
    print_separator_start('Initializing Ethereum network')
    process = subprocess.run(['docker',
                              'exec',
                              _eth_docker_conatiner.short_id,
                              'sh',
                              '-c',
                              './init-network.sh'],
                             stdout=STD_OUT,
                             stderr=ERR_OUT,
                             universal_newlines=True)
    print_separator_end('Ethereum network initialized')


def start_hive():
    print_separator_start(
        'Starting Hive docker container and waiting for the Generated block')
    start_docker([HIVE_DOCKER_IMAGE_NAME], 'Generated')
    hive_docker_container = get_docker_container_by_image_name(
        HIVE_DOCKER_IMAGE_NAME)
    print_separator_end(
        'Docker Hive started, docker id is ' +
        hive_docker_container.short_id)
    return hive_docker_container


def init_network_hive(_hive_docker_container):
    print_separator_start('Initializing Hive network')
    process = subprocess.run(['docker',
                              'exec',
                              _hive_docker_container.short_id,
                              'sh',
                              '-c',
                              './init-network.sh'],
                             stdout=STD_OUT,
                             stderr=ERR_OUT,
                             universal_newlines=True)
    print_separator_end('Hive network initialized')


def start_pp(_all_in_one=False):
    print_separator_start(
        'Starting Peerplays docker containers and waiting for the Generated block')
    image_names = [
        PP_DOCKER_IMAGE_NAME_ALL_IN_ONE] if _all_in_one else PP_DOCKER_IMAGE_NAMES_MULTI_NODE
    start_docker(image_names)
    pp_docker_container = get_docker_container_by_image_name(image_names[0])
    print_separator_end(
        'Docker Peerplays started, docker id is ' +
        pp_docker_container.short_id)
    return pp_docker_container


def init_network_pp(_pp_docker_container):
    print_separator_start('Initializing Peerplays network')
    process = subprocess.run(['docker',
                              'exec',
                              _pp_docker_container.short_id,
                              'sh',
                              '-c',
                              './init-network.sh'],
                             stdout=STD_OUT,
                             stderr=ERR_OUT,
                             universal_newlines=True)
    print_separator_end(
        'Peerplays network initialized')
