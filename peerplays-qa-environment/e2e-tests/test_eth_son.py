# SON-QA-Reference.xlsx, sheet "SON-for-Hive"

import pytest

from string_utils import *
from pp_utils import *
from ethereum_utils import *

ACCOUNT_PP = "account01"


# Verify Ethereum Deposit
def test_eth_deposit():
    # Setup deposit parameters
    deposit_amount = 2

    # Find out SON-related addresses
    sidechain_deposit_addr = pp_get_sidechain_address_by_account_and_sidechain(
        ACCOUNT_PP, "ethereum")["deposit_address"]
    pw_addr = pp_get_active_son_primary_wallet_address("ethereum")

    # Save initial balances
    balance0 = pp_get_account_balance(ACCOUNT_PP, "ETH")
    pw_balance0 = eth_call(pw_addr)
    eth_balance0 = eth_getBalance(sidechain_deposit_addr)

    # Save gas price before deposit transaction
    gasPrice = eth_gasPrice()

    # Prepare for the deposit transfer
    eth_unlock_account(sidechain_deposit_addr)

    # Make the deposit itself
    transHash = eth_sendTransaction(
        sidechain_deposit_addr,
        pw_addr,
        deposit_amount *
        ETH_PRECISION)

    # Wait for complete transaction handling
    assert eth_waitTransaction(transHash)
    pp_wait_for_operations_complete()
    time.sleep(1)
    pp_wait_for_operations_complete()
    time.sleep(1)
    pp_wait_for_operations_complete()
    time.sleep(40)

    # Once complete, can know the spent gas amount
    gasUsed = eth_hex_to_int(eth_getTransactionReceipt(transHash)["gasUsed"])

    # Get new balances
    balance1 = pp_get_account_balance(ACCOUNT_PP, "ETH")
    pw_balance1 = eth_call(pw_addr)
    eth_balance1 = eth_getBalance(sidechain_deposit_addr)

    # Uncomment to see balances concisely
    print("\n!!!DEP: gasPrice={}\ngasUsed={}\nbalance0=    {:>30},\nbalance1=    {:>30},\ndBalance=    {:>30},\neth_balance0={:>30},\neth_balance1={:>30},\neth_dBalance={:>30},\npw_balance0= {:>30},\npw_balance1= {:>30},\npw_dBalance= {:>30},\nval=         {:>30}".format(
        gasPrice, gasUsed, balance0, balance1, balance1 - balance0, eth_balance0, eth_balance1, eth_balance1 - eth_balance0, pw_balance0, pw_balance1, pw_balance1 - pw_balance0, deposit_amount))

    # Verify proper balance changes
    assert balance1 == balance0 + deposit_amount
    assert pw_balance1 == pw_balance0 + deposit_amount * ETH_PRECISION
    assert eth_balance1 == eth_balance0 - \
        deposit_amount * ETH_PRECISION - gasUsed * gasPrice


# Verify Ethereum Withdrawal
def test_eth_withdrawal():
    # Setup withdrawal parameters
    withdraw_amount = 1

    # Find out SON-related addresses
    sidechain_withdraw_addr = pp_get_sidechain_address_by_account_and_sidechain(
        ACCOUNT_PP, "ethereum")["withdraw_address"]
    pw_addr = pp_get_active_son_primary_wallet_address("ethereum")

    # Save initial balances
    balance0 = pp_get_account_balance(ACCOUNT_PP, "ETH")
    pw_balance0 = eth_call(pw_addr)
    eth_balance0 = eth_getBalance(sidechain_withdraw_addr)

    # Save gas price before transaction
    gasPrice = eth_gasPrice()
    gasUsed = 0

    # Make the withdrawal itself
    pp_transfer(ACCOUNT_PP, "son-account", withdraw_amount, "ETH")

    # Wait for complete transaction handling
    pp_wait_for_operations_complete()
    time.sleep(1)
    pp_wait_for_operations_complete()
    time.sleep(1)
    pp_wait_for_operations_complete()
    time.sleep(50)

    # Get new balances
    balance1 = pp_get_account_balance(ACCOUNT_PP, "ETH")
    pw_balance1 = eth_call(pw_addr)
    eth_balance1 = eth_getBalance(sidechain_withdraw_addr)

    # Uncomment to see balances concisely
    print("\n!!!WTD: gasPrice={}\ngasUsed={}\nbalance0=    {:>30},\nbalance1=    {:>30},\ndBalance=    {:>30},\neth_balance0={:>30},\neth_balance1={:>30},\neth_dBalance={:>30},\npw_balance0= {:>30},\npw_balance1= {:>30},\npw_dBalance= {:>30},\nval=         {:>30}".format(
        gasPrice, gasUsed, balance0, balance1, balance1 - balance0, eth_balance0, eth_balance1, eth_balance1 - eth_balance0, pw_balance0, pw_balance1, pw_balance1 - pw_balance0, withdraw_amount))

    # Verify proper balance changes
    assert balance1 == balance0 - withdraw_amount
    assert pw_balance1 == pw_balance0 - withdraw_amount * ETH_PRECISION
    assert eth_balance1 == eth_balance0 + withdraw_amount * ETH_PRECISION
