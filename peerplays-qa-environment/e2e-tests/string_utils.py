#!/usr/bin/python3

import random
import string

TIME_FORMAT = "%Y-%m-%dT%H:%M:%S"
CMD_LOG_PREFIX = "\n*************** "


def print_separator_start(_message):
    blanks = int((90 - len(_message)) / 2)
    just_message = _message.rjust(blanks + len(_message)).ljust(90)

    print("\n=====" + just_message + "=====")


def print_separator_end(_message):
    blanks = int((90 - len(_message)) / 2)
    just_message = _message.rjust(blanks + len(_message)).ljust(90)

    print("-----" + just_message + "-----\n")


def get_random_name(_length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(_length))


tolerate_rerun = False


def get_rerun_toleration_suffix(_length=3):
    return get_random_name(_length) if tolerate_rerun else ""
