import pytest

from string_utils import *
from hive_utils import *
from expected_values import *

@pytest.mark.order(1)
def test_create_accounts():
    all_accounts_names = hive_list_accounts("", 100)
    for expected_account in HIVE_EXPECTED_SON_ACCOUNTS + HIVE_EXPECTED_REGULAR_ACCOUNTS:
        assert expected_account[0] in all_accounts_names
        account = hive_get_account(expected_account[0])
        assert account["name"] == expected_account[0]

        key_types = ["owner", "active", "posting"]
        for i, key_type in enumerate(key_types):
            key = account[key_type]
            key_auths = key["key_auths"]

            assert key["weight_threshold"] == 1
            assert key["account_auths"] == []
            assert len(key_auths) == 1
            assert key_auths[0][0] == expected_account[i + 1]

        assert account["memo_key"] == expected_account[4]


@pytest.mark.order(1)
def test_key_pairs():
    all_keys = hive_list_keys()
    for key in all_keys:
        assert key[1] == HIVE_EXPECTED_KEY_PAIRS[key[0]]


@pytest.mark.order(1)
def test_balances():
    for expected_account in PP_EXPECTED_SON_ACCOUNTS + HIVE_EXPECTED_REGULAR_ACCOUNTS:
        account = hive_get_account(expected_account[0])
        assert account["balance"] == HIVE_EXPECTED_BALANCE
        assert account["hbd_balance"] == HIVE_EXPECTED_HBD_BALANCE
