# About

This module will be used for e2e testing. The e2e tests will be python based. In order to support automated tests, the module is also providing methods to manipulate docker containers and blockchains from pyhon code.

For now, the availabe fuctionalities are
* start docker continers
* stop docker containers
* run output commands for Bitcoin, Hive and Peerplays

# Requirements

The required modules can be found in the requirements.txt file and those can be installed by calling:
```
pip3 install -r requirements.txt
```

# Run

All available operations are started thoughout main.py scrip using python command line flag options.

## Start all docker containers (default mode for Peerplays is multinode mode)
```
./main.py --start all
```

## Start all docker containers (Peerplays in all-in-one mode)
```
./main.py --start all --all-in-one
```

## Start only certain docker containers
```
./main.py --start btc hive
```

## Stop docker containers
```
./main.py --stop
```

## Run Bitcoin output functions
```
./main.py --btc
```

## Run Peerplays output functions
```
./main.py --pp
```

## Run Hive output funcitons
```
./main.py --stop
```

## All commands

In the end, the upper flags can be combined so it is possible to call something like this:
```
./main.py --start all --btc --hive --pp --stop
```

# Run python tests

Before running automated tests make sure to start docker containers (see above). Automated python tests can be run by calling:
```
pytest
```
For now there is only one python tests which is testing the inital state of the systems after starting docker containers.

Single test set (test file) can be run using:
```
pytest <test_file>.py
```

Single test (one test from one file) can be run using:
```
pytest <test_file>.py::<test_name>
```

To see all commands and output, add '-s' switch:
```
pytest -s <test_file>.py::<test_name>
```

# Troubleshoot

## Permission denied when running main.py
```
bash: ./main.py: Permission denied
```

This message is reported when there are no execute requirements granted for the current user main.py script. This can be fixed by calling:
```
chmod u+x ./main.py
```
