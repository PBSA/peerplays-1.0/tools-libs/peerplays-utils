#!/usr/bin/python3

import json


def convert_byte_output_to_json(_byte_output):
    try:
        json_output = json.loads(_byte_output.decode('utf8').replace("'", '"'))
    except json.decoder.JSONDecodeError as e:
        print(_byte_output)
        raise e
    return json_output


def convert_string_to_json(_string):
    try:
        json_output = json.loads(_string)
    except json.decoder.JSONDecodeError as e:
        print(_string)
        raise e
    return json_output
