# SON-QA-Reference.xlsx, sheet "SON-Generic"

import pytest

from string_utils import *
from pp_utils import *
from bitcoin_utils import *

ACCOUNT = "test1" + get_rerun_toleration_suffix()
SON_URL = "http://test1.com"


# SON-227: Create SON using create_son command
def test_create_son():
    acc = pp_create_new_account(BRAIN_KEY, ACCOUNT, "nathan", "nathan")

    pp_transfer("nathan", ACCOUNT, 20000, "TEST", "")

    pp_wait_for_operations_complete()

    pp_upgrade_account(ACCOUNT)

    pp_create_vesting_balance(ACCOUNT, 50, "TEST", "son")
    pp_create_vesting_balance(ACCOUNT, 50, "TEST", "normal")
    vbs = pp_get_vesting_balances(ACCOUNT)
    vb_ids_son = [vb["id"] for vb in vbs if vb["balance_type"] == "son"]
    assert len(vb_ids_son) == 1
    [son_vesting_object_id] = vb_ids_son
    vb_ids_normal = [vb["id"] for vb in vbs if vb["balance_type"] == "normal"]
    assert len(vb_ids_normal) == 1
    [normal_vesting_object_id] = vb_ids_normal

    pp_create_son(
        ACCOUNT,
        SON_URL,
        son_vesting_object_id,
        normal_vesting_object_id)

    son = pp_get_son(ACCOUNT)

    # Verify that the status of newly created account is Inactive
    statuses = son["statuses"]
    assert len(statuses) > 0
    active_statuses = [st for st in statuses if st[1] == "active"]
    assert len(active_statuses) == 0

    # Verify the signing key of newly created SON: it should be the same as
    # SON owner account's active public key
    signing_key = son["signing_key"]
    assert len(signing_key) > 0
    acc = pp_get_account(ACCOUNT)
    active_public_key = acc["active"]["key_auths"][0][0]
    assert signing_key == active_public_key


# SON-228: Update SON using update_son command
def test_update_son():
    # Save initial SON properties
    son = pp_get_son(ACCOUNT)
    url0 = son["url"]
    signing_key0 = son["signing_key"]
    sidechain_public_keys0 = son["sidechain_public_keys"]

    # Devise new SON properties
    url1 = "http://updatedurl.com"
    assert url1 != url0
    signing_key1 = "TEST6Yaq5ZNTTkMM2kBBzV5jktr8ETsniCC3bnVD7eFmegRrLXfGGG"
    assert signing_key1 != signing_key0
    sidechain_public_keys1 = [
        ["bitcoin", "bitcoin_address 2"], ["hive", "hive account 2"]]
    assert sidechain_public_keys1 != sidechain_public_keys0

    # Update URL only
    pp_update_son(ACCOUNT, url1)
    son = pp_get_son(ACCOUNT)
    assert son["url"] == url1
    assert son["signing_key"] == signing_key0
    assert son["sidechain_public_keys"] == sidechain_public_keys0

    # Update signing key only
    pp_update_son(ACCOUNT, "", signing_key1)
    son = pp_get_son(ACCOUNT)
    assert son["url"] == url1
    assert son["signing_key"] == signing_key1
    assert son["sidechain_public_keys"] == sidechain_public_keys0

    # Update sidechain public keys only
    pp_update_son(ACCOUNT, "", "", sidechain_public_keys1)
    son = pp_get_son(ACCOUNT)
    assert son["url"] == url1
    assert son["signing_key"] == signing_key1
    assert son["sidechain_public_keys"] == sidechain_public_keys1


# SON-230: Vote for SON
def test_vote_for_son():
    voted_son = "sonaccount01"
    voting_account = ACCOUNT

    # Ensure some vesting balance
    pp_create_vesting_balance(voting_account, 50, "TEST", "gpos")
    pp_wait_for_operations_complete()
    vesting_balance = pp_get_total_vesting_balance(voting_account, "gpos")
    assert vesting_balance > 0

    # Save initial votes
    votes0 = pp_get_total_votes_for_son(voted_son, "bitcoin")

    # Vote and wait for actualization
    pp_vote_for_son(voting_account, voted_son, "bitcoin", True)
    pp_wait_for_maintenance_block()

    # Check votes increase
    votes1 = pp_get_total_votes_for_son(voted_son, "bitcoin")
    assert votes1 == votes0 + vesting_balance


# SON-235: Unvote SON
def test_unvote_son():
    voted_son = "sonaccount01"
    voting_account = ACCOUNT

    # Ensure some vesting balance
    vesting_balance = pp_get_total_vesting_balance(voting_account, "gpos")
    assert vesting_balance > 0

    # Save initial votes
    votes0 = pp_get_total_votes_for_son(voted_son, "bitcoin")

    # Unvote and wait for actualization
    pp_vote_for_son(voting_account, voted_son, "bitcoin", False)
    pp_wait_for_maintenance_block()

    # Check votes decrease
    votes1 = pp_get_total_votes_for_son(voted_son, "bitcoin")
    assert votes1 == votes0 - vesting_balance


# SON-244: Verify SON sends heartbeat at regular interval
def test_son_heartbeat():
    son = pp_get_son("sonaccount01")

    # Fetch all statuses and derive available sidechain types
    statuses = son["statuses"]
    sidechains = [st[0] for st in statuses]
    assert len(sidechains) > 0

    # Verify that all statuses are active
    status_dict = pp_2d_list_to_dict(statuses)
    for sc in sidechains:
        assert status_dict.get(sc) == "active"

    # To access statistics
    stats_obj_id = son["statistics"]

    # Get current time
    cur_time = pp_get_current_time()

    # Save initial active timestamps
    timestamp_dict = pp_2d_list_to_dict(
        pp_get_object(stats_obj_id)[0]["last_active_timestamp"])
    timestamps0 = [
        datetime.strptime(
            timestamp_dict.get(sc),
            TIME_FORMAT) for sc in sidechains]

    # Check that all timestamps are not older than 3 min. before the current
    # time
    for ts in timestamps0:
        assert (cur_time - ts).total_seconds() <= 3 * 60

    # Wait 3 minutes
    time.sleep(3 * 60)

    # Save new active timestamps
    timestamp_dict = pp_2d_list_to_dict(
        pp_get_object(stats_obj_id)[0]["last_active_timestamp"])
    timestamps1 = [
        datetime.strptime(
            timestamp_dict.get(sc),
            TIME_FORMAT) for sc in sidechains]

    # Check that all timestamps are not older than 3 min. before the current
    # time
    for ts in timestamps1:
        assert (cur_time - ts).total_seconds() <= 3 * 60

    # Check that new timestamps differ from the initial ones by not less than
    # 2 minutes
    for i in range(len(timestamps1)):
        assert (timestamps1[i] - timestamps0[i]).total_seconds() >= 2 * 60


def son_id_2_account_name(_son_id):
    return pp_get_account(pp_get_son(_son_id)["son_account"])["name"]


# SON-245: Verify list_active_sons command
def test_active_son_listing():
    # We'll change the list of active SONs my means of voting and unvoting

    # Get all active SONs
    active_son_dict = pp_2d_list_to_dict(pp_get_active_sons())
    # and active Bitcoin SONs
    active_sons_btc = active_son_dict["bitcoin"]

    # There must be 15 active SONs in total
    arr = []
    for x in ("bitcoin", "hive", "ethereum"):
        arr = arr + active_son_dict[x]
    assert len(arr) == 15
    # 5 of them are Bitcoin
    assert len(active_sons_btc) == 5

    # Decide for which SONs to unvote and vote
    # total theoretically available
    total_son_ids = ["1.33.{}".format(n) for n in range(16)]
    active_son_ids = [son["son_id"] for son in active_sons_btc]  # all active
    # active must be subset of total
    assert all(id in total_son_ids for id in active_son_ids)
    nonactive_son_ids = [
        id for id in total_son_ids if id not in active_son_ids]  # total
    assert len(active_son_ids) > 0
    unvoted_son_id = active_son_ids[-1]  # last of active SONs
    assert len(nonactive_son_ids) > 0
    voted_son_id = nonactive_son_ids[0]  # any inactive SON
    # derive SON account names from SON ids
    unvoted_son_account = son_id_2_account_name(unvoted_son_id)
    voted_son_account = son_id_2_account_name(voted_son_id)

    # He will unvote one SON and vote for another
    voting_account = ACCOUNT

    # Ensure some vesting balance
    pp_create_vesting_balance(voting_account, 50, "TEST", "gpos")
    pp_wait_for_operations_complete()
    vesting_balance = pp_get_total_vesting_balance(voting_account, "gpos")
    assert vesting_balance > 0

    # Don't unvote "unvoted_son_account" since usually it has zero votes and unvoting throws an exception.
    # Vote for another.
    pp_vote_for_son(voting_account, voted_son_account, "bitcoin", True)
    # Wait for voting to have an effect
    pp_wait_for_maintenance_block()

    # Check the state after voting.
    # First, get all active Bitcoin SONs again
    active_son_ids = [
        son["son_id"] for son in pp_2d_list_to_dict(
            pp_get_active_sons())["bitcoin"]]
    # active must be subset of total
    assert all(id in total_son_ids for id in active_son_ids)
    # Check that unvoted one goes away
    assert unvoted_son_id not in active_son_ids
    # and the voted one appears
    assert voted_son_id in active_son_ids

    # Reverse voting
    pp_vote_for_son(voting_account, voted_son_account, "bitcoin", False)
    pp_vote_for_son(voting_account, unvoted_son_account, "bitcoin", True)
    # Wait for voting to have an effect
    pp_wait_for_maintenance_block()

    # Check the state after reverse voting.
    active_son_ids = [
        son["son_id"] for son in pp_2d_list_to_dict(
            pp_get_active_sons())["bitcoin"]]

    # Check that initially unvoted is back
    assert unvoted_son_id in active_son_ids
    # and the initially voted one disappears
    assert voted_son_id not in active_son_ids


# SON-243: Report SON down
def test_report_son_down():
    # Account name of SON under test
    account_name = "sonaccount02"

    # Get all active Bitcoin SON IDs
    active_son_ids = [
        son["son_id"] for son in pp_2d_list_to_dict(
            pp_get_active_sons())["bitcoin"]]
    assert len(active_son_ids) > 0

    # Get SON properties
    son = pp_get_son(account_name)

    # Initial assertions on the SON:
    assert son["id"] in active_son_ids  # must be among active ones
    # Status must be "active"
    status = pp_2d_list_to_dict(son["statuses"])["bitcoin"]
    assert status == "active"

    # Get statistics object ID
    stats_obj_id = son["statistics"]

    # Check that last active timestamp is within last 3 minutes (1 heartbeat)
    # from current time
    cur_time = pp_get_current_time()
    last_active_timestamp = datetime.strptime(pp_2d_list_to_dict(pp_get_object(
        stats_obj_id)[0]["last_active_timestamp"])["bitcoin"], TIME_FORMAT)
    assert (cur_time - last_active_timestamp).total_seconds() <= 3 * 60 + 20

    # Stop docker container serving "account_name"
    stop_docker("peerplays02")

    # Wait for next 2 heartbeats to happen (currently 1 heartbeat = 3 mins)
    time.sleep(4 * 3 * 60 + 10)

    # Get SON properties again
    son = pp_get_son(account_name)

    # Now status must become "in_maintenance"
    status = pp_2d_list_to_dict(son["statuses"])["bitcoin"]
    assert status == "in_maintenance"

    # Ensure recent history contains information about the operation
    hist = pp_get_account_history("son-account", 10)
    assert hist.find("son_report_down_operation son-account fee: 0 TEST") != -1

    for i in range(16):
        pp_get_account_history("sonaccount{:02d}".format(i + 1), 10)


# SON-246: Verify duplicate SON proposals are not created
def test_duplicate_son_down_proposals():
    # Ensure recent history contains one and only one line about SON down
    # operation
    hist = pp_get_account_history("son-account", 100).splitlines()
    assert len([h for h in hist if h.find(
        "son_report_down_operation") != -1]) == 1


# SON-247: Verify request_son_maintenance and
# cancel_request_son_maintenance commands
def test_son_maintenance_request_and_cancel():
    pp_get_global_properties()
    return
    # Account name of SON under test
    account_name = "sonaccount03"

    # Get SON properties
    son = pp_get_son(account_name)

    pp_request_son_maintenance(account_name)
