# SON-QA-Reference.xlsx, sheet "SON-for-Bitcoin"

import pytest

from string_utils import *
from pp_utils import *
from bitcoin_utils import *

ACCOUNT_PP = "account02"


# SON-272: Verify Bitcoin Deposit
def test_btc_deposit():
    # Setup deposit parameters
    deposit_amount = 30

    # Find out SON-related addresses
    sidechain_deposit_addr = pp_get_sidechain_address_by_account_and_sidechain(
        ACCOUNT_PP, "bitcoin")["deposit_address"]
    pw_addr = pp_get_active_son_primary_wallet_address("bitcoin")

    # Save initial balances
    balance0 = pp_get_account_balance(ACCOUNT_PP, "BTC")
    btc_balance0 = btc_getbalance()
    pw_balance0 = btc_listunspent_amount_per_address(pw_addr, "son-wallet")

    # Estimate the fee
    btc_transaction_fee = btc_estimatesmartfee()

    # Make the deposit itself
    btc_sendtoaddress(sidechain_deposit_addr, deposit_amount)

    # Wait for complete transaction handling
    btc_generatetoaddress(BTC_GENERATION_ADDR)
    time.sleep(15)
    btc_generatetoaddress(BTC_GENERATION_ADDR)
    time.sleep(20)

    # Get new balances
    balance1 = pp_get_account_balance(ACCOUNT_PP, "BTC")
    btc_balance1 = btc_getbalance()
    pw_balance1 = btc_listunspent_amount_per_address(pw_addr, "son-wallet")

    # Verify proper increases
    assert round(balance1 * BTC_PRECISION) == round(balance0 * BTC_PRECISION) + \
        deposit_amount * BTC_PRECISION - btc_transaction_fee * BTC_PRECISION
    assert round(pw_balance1 * BTC_PRECISION) == round(pw_balance0 * BTC_PRECISION) + \
        deposit_amount * BTC_PRECISION - btc_transaction_fee * BTC_PRECISION


# SON-302: Verify Bitcoin Withdrawal
def test_btc_withdrawal():
    # Setup withdrawal parameters
    withdraw_amount = 20

    # Find out SON-related addresses
    sidechain_withdraw_addr = pp_get_sidechain_address_by_account_and_sidechain(
        ACCOUNT_PP, "bitcoin")["withdraw_address"]

    # Save initial balances
    balance0 = pp_get_account_balance(ACCOUNT_PP, "BTC")
    son_balance0 = pp_get_account_balance("son-account", "BTC")
    btc_balance0 = btc_listunspent_amount_per_address(sidechain_withdraw_addr)

    # Estimate the fee
    btc_transaction_fee = btc_estimatesmartfee()

    # Make the withdrawal itself
    pp_transfer(ACCOUNT_PP, "son-account", withdraw_amount, "BTC")

    # Get new balances - first, before bitcoin block generation
    balance1 = pp_get_account_balance(ACCOUNT_PP, "BTC")
    son_balance1 = pp_get_account_balance("son-account", "BTC")

    # Verify proper balance changes
    assert round(balance1 * BTC_PRECISION) == round(balance0 * \
                 BTC_PRECISION) - withdraw_amount * BTC_PRECISION
    assert round(son_balance1 * BTC_PRECISION) == round(son_balance0 *
                                                        BTC_PRECISION) + withdraw_amount * BTC_PRECISION

    # Wait for complete transaction handling
    btc_generatetoaddress(BTC_GENERATION_ADDR)
    time.sleep(15)
    btc_generatetoaddress(BTC_GENERATION_ADDR)
    time.sleep(20)

    # Get new balance after bitcoin block generation
    btc_balance1 = btc_listunspent_amount_per_address(sidechain_withdraw_addr)

    # Verify proper balance increase
    assert round(btc_balance1 * BTC_PRECISION) == round(btc_balance0 * BTC_PRECISION) + \
        withdraw_amount * BTC_PRECISION - btc_transaction_fee * BTC_PRECISION
