#!/usr/bin/python3

import argparse
import datetime


from docker_utils import *
from bitcoin_utils import *
from hive_utils import *
from pp_utils import *


def setup_parser():
    # Setup CLI arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--stop", action='store_true',
                        help="Stop all docker containers")
    parser.add_argument("--start", nargs="*", type=str, default=[],
                        help="Start all docker containers")
    parser.add_argument("--btc", action='store_true',
                        help="Start all calls for BTC")
    parser.add_argument("--hive", action='store_true',
                        help="Start all calls for Hive")
    parser.add_argument("--pp", action='store_true',
                        help="Start all calls for Peerplays")
    parser.add_argument("--all-in-one", action='store_true', dest='all_in_one',
                        help="Start peerplays in all-in-one mode")

    return parser.parse_args()

################################# MAIN ###################################


args = setup_parser()

if args.stop:
    stop_all_dockers()

if args.start:
    container_aliases = []
    for a in args.start:
        if a == 'all':
            container_aliases.extend(('btc', 'eth', 'hive', 'pp'))
        else:
            container_aliases.append(a)

    if 'btc' in container_aliases:
        btc_docker_container = start_btc()
        init_network_btc(btc_docker_container)

    if 'eth' in container_aliases:
        eth_docker_conatiner = start_eth()
        init_network_eth(eth_docker_conatiner)

    if 'hive' in container_aliases:
        hive_docker_container = start_hive()
        init_network_hive(hive_docker_container)

    if 'pp' in container_aliases:
        pp_docker_container = start_pp(_all_in_one=args.all_in_one)
        pp_wait_for_block_number_before_init(10)
        init_network_pp(pp_docker_container)
        pp_wait_for_maintenance_block()

if args.btc:
    btc_dumpprivkey("2MtTPtraZawsvNGc8eCdx98hXbi4gaYy8L6")
    btc_dumpprivkey('\"2MtTPtraZawsvNGc8eCdx98hXbi4gaYy8L6\"')
    btc_listwallets()
    print(btc_getbalance())
    btc_getbalances()
    btc_getpeerinfo()
    btc_getblockcount()
    btc_getmininginfo()
    btc_getnetworkinfo()
    btc_getblockchaininfo()
    btc_estimatesmartfee()
    btc_getunconfirmedbalance()
    btc_getmempoolinfo()
    btc_listunspent_per_address(
        '\"2N8trY5h3febNQB2V3vphKhshQsotuJ5XDU\"')

if args.hive:
    hive_about()
    hive_info()


if args.pp:
    pp_get_global_properties()
    pp_get_dynamic_global_properties()
    pp_info()
    pp_about()
    pp_list_account_balances("account01")
