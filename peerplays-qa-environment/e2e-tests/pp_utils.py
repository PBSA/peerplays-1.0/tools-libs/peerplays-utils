#!/usr/bin/python3

import docker
import pexpect
import re
import requests

from datetime import datetime, timedelta

from docker_utils import *
from json_utils import *
from string_utils import *

PP_PRECISION = 100000

PP_CLI_WALLET = None
URL = "http://10.11.12.101:8090"

BRAIN_KEY = "CHOROGI EERIE RETUCK PRAECOX MUDDLER LITERAL ACRON CARBRO BABBY AGAZED UNBOLT ABASED HALA TEMBLOR EMANATE HEMIPIC"


def init_pp_cli_wallet(_chain_id):
    pp_docker_container = None
    try:
        pp_docker_container = get_docker_container_by_image_name(
            PP_DOCKER_IMAGE_NAMES_MULTI_NODE[0])
    except RuntimeError:
        try:
            pp_docker_container = get_docker_container_by_image_name(
                PP_DOCKER_IMAGE_NAME_ALL_IN_ONE)
        except RuntimeError:
            print(
                "\nNeither {} nor {} Peerplays containers found, falling back to baremetal cli_wallet\n".format(
                    PP_DOCKER_IMAGE_NAMES_MULTI_NODE[0],
                    PP_DOCKER_IMAGE_NAME_ALL_IN_ONE))

    pp_cli_wallet = pexpect.spawn(
        ('docker exec -it ' +
         pp_docker_container.short_id +
         ' ' if pp_docker_container is not None else '') +
        './cli_wallet --chain-id=' +
        _chain_id,
        cwd=None if pp_docker_container else "/home/bunker/devel")
    pp_cli_wallet.expect('locked >>> ')
    pp_cli_wallet.sendline('unlock password')
    pp_cli_wallet.expect('unlocked >>> ')
    return pp_cli_wallet


def pp_cli_wallet_send_command(_command, _convert_response_to_json=True):
    global PP_CLI_WALLET
    if PP_CLI_WALLET is None:
        PP_CLI_WALLET = init_pp_cli_wallet(
            "7c1c72eb738b3ff1870350f85daca27e2d0f5dd25af27df7475fbd92815e421e")

    PP_CLI_WALLET.sendline(_command)
    PP_CLI_WALLET.expect('unlocked >>> ')
    resp = PP_CLI_WALLET.before.decode("utf-8")
    resp = re.sub(' \b', '', resp)  # get rid of space+backspace sequences

    # Trim the beginning of the response
    split_lines = resp.splitlines()
    start = 0
    while start < len(split_lines):
        # It can be a new line or a printout of the command
        if (len(split_lines[start]) == 0) or (
                split_lines[start].find(_command) != -1):
            start += 1
        # There can also be a noise from the wallet which spans to 2 lines
        elif "wallet.cpp" in split_lines[start]:
            start += 2
        else:
            break

    stop = start
    while stop < len(split_lines):
        if "state.cpp" in split_lines[stop]:
            break
        stop += 1

    response = '\n'.join(split_lines[start:stop])
    print(CMD_LOG_PREFIX +
          "Peerplays command:\n{}\n-->\n{}\n".format(_command, response))
    if _convert_response_to_json:
        return convert_string_to_json(response)
    else:
        return response


def pp_get_head_block_number_via_http():
    req = {
        "jsonrpc": "2.0",
        "method": "get_dynamic_global_properties",
        "params": [],
        "id": 1}
    json_obj = convert_byte_output_to_json(
        requests.post(URL, json=req).content)
    return json_obj["result"]["head_block_number"]


def pp_wait_for_block_number_before_init(_num):
    while pp_get_head_block_number_via_http() < _num:
        time.sleep(3)


def pp_2d_list_to_dict(_list):
    _dict = {}
    for elem in _list:
        _dict[elem[0]] = elem[1]
    return _dict


def pp_about():
    return pp_cli_wallet_send_command('about')


def pp_info():
    return pp_cli_wallet_send_command('info')


def pp_get_object(_object_id):
    return pp_cli_wallet_send_command('get_object {}'.format(_object_id))


def pp_get_dynamic_global_properties():
    return pp_cli_wallet_send_command('get_dynamic_global_properties')


def pp_get_global_properties():
    return pp_cli_wallet_send_command('get_global_properties')


def pp_create_new_account(_brain_key, _account_name, _registrar, _referrer):
    return pp_cli_wallet_send_command(
        'create_account_with_brain_key "{}" {} {} {} true'.format(
            _brain_key, _account_name, _registrar, _referrer))


def pp_get_account(_account_name):
    return pp_cli_wallet_send_command('get_account {}'.format(_account_name))


def pp_suggest_brain_key():
    return pp_cli_wallet_send_command("suggest_brain_key")


def pp_create_new_account_by_name(_account_name):
    brain_key = pp_suggest_brain_key()["brain_priv_key"]
    return pp_create_new_account(
        brain_key, _account_name, "nathan", "nathan")


def pp_list_accounts(_lower_bound, _limit):
    return pp_cli_wallet_send_command(
        'list_accounts "{}" {}'.format(
            _lower_bound, _limit))


def pp_upgrade_account(_account_name):
    return pp_cli_wallet_send_command(
        'upgrade_account {} true'.format(_account_name))


def pp_list_witnesses(_lower_bound, _limit):
    return pp_cli_wallet_send_command(
        'list_witnesses "{}" {}'.format(
            _lower_bound, _limit))


def pp_get_witness(_witness_name):
    return pp_cli_wallet_send_command('get_witness ' + _witness_name)


def pp_create_son(
        _account_name,
        _url,
        _son_vesting_object_id,
        _normal_vesting_object_id):
    return pp_cli_wallet_send_command('create_son {} "{}" {} {} [] true'.format(
        _account_name, _url, _son_vesting_object_id, _normal_vesting_object_id))


def pp_update_son(
        _account_name,
        _url,
        _block_signing_key='',
        _sidechain_public_keys=[]):
    return pp_cli_wallet_send_command('update_son {} "{}" "{}" {} true'.format(
        _account_name,
        _url,
        _block_signing_key,
        str(_sidechain_public_keys).replace(
            "'",
            '"')))


def pp_list_sons(_lower_bound, _limit):
    return pp_cli_wallet_send_command(
        'list_sons "{}" {}'.format(
            _lower_bound, _limit))


def pp_get_active_sons():
    return pp_cli_wallet_send_command(
        'get_active_sons')


def pp_get_son(_son_name):
    return pp_cli_wallet_send_command('get_son ' + _son_name)


def pp_get_total_votes_for_son(_son_account, _sidechain):
    return pp_2d_list_to_dict(
        pp_get_son(_son_account)["total_votes"]).get(
        _sidechain, 0)


def pp_get_total_vesting_balance(_account, _type):
    return sum(bal["balance"]["amount"] for bal in pp_get_vesting_balances(
        _account) if bal["balance_type"] == _type)


def pp_list_active_witnesses():
    return pp_get_global_properties()["active_witnesses"]


def pp_vote_for_witness(_voter, _votey):
    return pp_cli_wallet_send_command(
        'vote_for_witness {} {} true true'.format(
            _voter, _votey))


def pp_vote_for_son(_voter, _votey, _sidechain, _approve):
    return pp_cli_wallet_send_command(
        'vote_for_son {} {} {} {} true'.format(
            _voter, _votey, _sidechain, str(_approve).lower()))


def pp_request_son_maintenance(_son_account):
    return pp_cli_wallet_send_command(
        'request_son_maintenance {} true'.format(_son_account))


def pp_cancel_request_son_maintenance(_son_account):
    return pp_cli_wallet_send_command(
        'cancel_request_son_maintenance {} true'.format(_son_account))


def pp_create_witness(_account_name, _website):
    return pp_cli_wallet_send_command(
        'create_witness {} "{}" true'.format(
            _account_name, _website))


def pp_create_vesting_balance(_owner, _amount, _asset_symbol, _type):
    return pp_cli_wallet_send_command(
        'create_vesting_balance {} {} {} {} true'.format(
            _owner, _amount, _asset_symbol, _type))


def pp_get_vesting_balances(_account_name):
    return pp_cli_wallet_send_command(
        'get_vesting_balances ' + _account_name)


def pp_transfer(_from, _to, _amount, _asset_symbol, _memo=""):
    return pp_cli_wallet_send_command('transfer {} {} {} {} "{}" true'.format(
        _from, _to, _amount, _asset_symbol, _memo))


def pp_get_account_history(_account_name, _limit):
    return pp_cli_wallet_send_command(
        'get_account_history {} {}'.format(
            _account_name, _limit), False)


def pp_get_asset(_asset):
    return pp_cli_wallet_send_command('get_asset ' + _asset)


def pp_list_account_balances(_account):
    dict_of_balances = {}
    response = pp_cli_wallet_send_command(
        'list_account_balances ' + _account,
        _convert_response_to_json=False)
    if response != "":
        split = response.rstrip().split("\n")
        # Each line contains one asset with its balance
        for line in split:
            # Split into list of two strings, balance and asset
            balance_and_asset = line.split(' ')
            # Balance is converted to int
            dict_of_balances.update(
                {balance_and_asset[1]: float(balance_and_asset[0])})
    return dict_of_balances


def pp_get_account_balance(_account, _asset_symbol):
    return pp_list_account_balances(_account).get(_asset_symbol, 0)


def pp_add_sidechain_address(
        _account,
        _sidechain,
        _deposit_public_key,
        _deposit_address,
        _withdraw_public_key,
        _withdraw_address):
    return pp_cli_wallet_send_command(
        'add_sidechain_address {} {} {} {} {} {} true'.format(
            _account,
            _sidechain,
            _deposit_public_key,
            _deposit_address,
            _withdraw_public_key,
            _withdraw_address))


def pp_delete_sidechain_address(_account, _sidechain):
    return pp_cli_wallet_send_command(
        'delete_sidechain_address {} {} true'.format(
            _account, _sidechain))


def pp_get_sidechain_addresses_by_account(_account):
    return pp_cli_wallet_send_command(
        'get_sidechain_addresses_by_account {}'.format(
            _account))


def pp_get_sidechain_address_by_account_and_sidechain(_account, _sidechain):
    return pp_cli_wallet_send_command(
        'get_sidechain_address_by_account_and_sidechain {} {}'.format(
            _account, _sidechain))


def pp_get_active_son_wallet():
    return pp_cli_wallet_send_command('get_active_son_wallet')


def pp_get_active_son_primary_wallet_addr_obj(_sidechain):
    return pp_2d_list_to_dict(pp_get_active_son_wallet()[
                              "addresses"])[_sidechain]


def pp_get_active_son_primary_wallet_address(_sidechain):
    addr_obj = pp_get_active_son_primary_wallet_addr_obj(_sidechain)
    if _sidechain == "bitcoin":
        addr = convert_string_to_json(addr_obj)["address"]
    else:
        addr = addr_obj
    return addr


def pp_get_object(_obj_name):
    return pp_cli_wallet_send_command('get_object ' + _obj_name)


def pp_get_head_block_num():
    return pp_info()["head_block_num"]


def pp_wait_for_operations_complete():
    head0 = pp_get_head_block_num()
    while pp_get_head_block_num() == head0:
        time.sleep(1)


def pp_propose_parameter_change(_account, _expiration_time, _changed_values):
    return pp_cli_wallet_send_command(
        'propose_parameter_change {} "{}" {} true'.format(
            _account, _expiration_time, _changed_values))


def pp_propose_gpos_change(_account, _gpos_period, _gpos_subperiod):
    changed_values = '{{"extensions":{{"gpos_period" : {}, "gpos_subperiod": {}}}}}'.format(
        _gpos_period, _gpos_subperiod)
    exp_time = pp_get_current_time() + timedelta(minutes=15)
    return pp_propose_parameter_change(
        _account, exp_time.strftime(TIME_FORMAT), changed_values)


def pp_get_current_time():
    return datetime.strptime(
        pp_get_dynamic_global_properties()["time"],
        TIME_FORMAT)


def pp_approve_proposal(_voter, _proposal_id):
    return pp_cli_wallet_send_command(
        'approve_proposal {} {} {{"active_approvals_to_add" : ["{}"]}} true'.format(
            _voter, _proposal_id, _voter))


def pp_get_next_maintenance_time():
    return datetime.strptime(
        pp_get_dynamic_global_properties()["next_maintenance_time"],
        TIME_FORMAT)


def pp_wait_for_maintenance_block():
    current_time = pp_get_current_time()
    next_maint_time = pp_get_next_maintenance_time()
    wait_time = next_maint_time - current_time
    time.sleep(wait_time.total_seconds() + 12)
    # don't rely on sleep above, ensure maintenance time actually changed
    while pp_get_next_maintenance_time() == next_maint_time:
        time.sleep(10)
    time.sleep(10)
