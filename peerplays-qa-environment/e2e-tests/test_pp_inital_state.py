import pytest

from string_utils import *
from pp_utils import *
from expected_values import *

@pytest.mark.order(1)
def test_create_accounts():
    # List all accounts and create a sublist taking only names (first element)
    all_accounts_names = [a[0] for a in pp_list_accounts("", 100)]

    for account in PP_EXPECTED_REGULAR_ACCOUNTS:
        assert account in all_accounts_names

    for account in PP_EXPECTED_SON_ACCOUNTS:
        son_name = account[0]
        assert son_name in all_accounts_names

    for account in PP_EXPECTED_WITNESS_ACCOUNTS:
        witness_name = account[0]
        assert witness_name in all_accounts_names


@pytest.mark.order(1)
def test_assets():
    for expected_asset in PP_EXPECTED_ASSETS:
        asset = pp_get_asset(expected_asset[0])
        assert asset["symbol"] == expected_asset[0]
        assert asset["precision"] == 5

        options = asset["options"]
        assert options["max_supply"] == '1000000000000000'
        assert options["market_fee_percent"] == 0
        assert options["max_market_fee"] == '1000000000000000'
        assert options["issuer_permissions"] == 79
        assert options["flags"] == 0
        assert options["description"] == ""
        assert options["extensions"] == []
        assert options["blacklist_authorities"] == []
        assert options["blacklist_markets"] == []
        assert options["whitelist_authorities"] == []
        assert options["whitelist_markets"] == []

        core_exchange_rate = options["core_exchange_rate"]
        assert core_exchange_rate["base"]["amount"] == 1
        assert core_exchange_rate["base"]["asset_id"] == "1.3.0"
        assert core_exchange_rate["quote"]["amount"] == expected_asset[1]


@pytest.mark.order(1)
def test_vesting_balances():
    for account in PP_EXPECTED_REGULAR_ACCOUNTS:
        vesting_balances = pp_get_vesting_balances(account)

        # Witness accounts and regular accounts should only have GPOS balances
        assert len(vesting_balances) == 1
        assert vesting_balances[0]["balance_type"] == "gpos"
        assert vesting_balances[0]["balance"]["amount"] == PP_EXPECTED_VESTING_BALANCE

    for account in PP_EXPECTED_WITNESS_ACCOUNTS:
        witness_name = account[0]
        vesting_balances = pp_get_vesting_balances(witness_name)

        # Witness accounts and regular accounts should only have GPOS balances
        assert len(vesting_balances) == 1
        assert vesting_balances[0]["balance_type"] == "gpos"
        assert vesting_balances[0]["balance"]["amount"] == PP_EXPECTED_VESTING_BALANCE

    for account in PP_EXPECTED_SON_ACCOUNTS:
        son_name = account[0]
        vesting_balances = pp_get_vesting_balances(son_name)

        # SON accounts should have GPOS, normal and SON balances
        assert len(vesting_balances) == 3
        remaining_balance_types = {"gpos", "normal", "son"}
        for vesting_balance in vesting_balances:
            assert vesting_balance["balance_type"] in remaining_balance_types
            remaining_balance_types.remove(vesting_balance["balance_type"])
            assert vesting_balance["balance"]["amount"] == PP_EXPECTED_VESTING_BALANCE


@pytest.mark.order(1)
def test_assets_balances():
    for account in PP_EXPECTED_REGULAR_ACCOUNTS:
        balances = pp_list_account_balances(account)

        # Regular accounts should have 4 balances: PBTC, POES, PETH and TEST
        assert len(balances) == 4
        assert balances["PBTC"] == 1000000
        assert balances["PEOS"] == 1000000
        assert balances["PETH"] == 1000000
        assert balances["TEST"] == 49999950

    for account in PP_EXPECTED_WITNESS_ACCOUNTS + PP_EXPECTED_SON_ACCOUNTS:
        balances = pp_list_account_balances(account[0])

        # Wintess and SON accounts only have TEST asset
        assert len(balances) == 1
        # Their balance varies, but it is always between 49999850 and 50000000
        assert balances["TEST"] >= 49999850 and balances["TEST"] <= 50000000


@pytest.mark.order(1)
def test_created_witnesses():
    # List all witnesses and create a sublist taking only names (first element)
    all_witnesses_names = [a[0] for a in pp_list_witnesses("", 100)]

    # Check witness exists and check its signing key
    for account in PP_EXPECTED_WITNESS_ACCOUNTS:
        assert account[0] in all_witnesses_names
        witness = pp_get_witness(account[0])
        assert witness["signing_key"] == account[1]


@pytest.mark.order(1)
def test_created_sons():
    # List all SONs and create a sublist taking only names (first element)
    all_sons_names = [a[0] for a in pp_list_sons("", 100)]

    for account in PP_EXPECTED_SON_ACCOUNTS:
        son_name = account[0]
        assert son_name in all_sons_names

        # Check sidechain keys
        btc_key = account[1]
        ethereum_key = account[2]
        hive_key = account[3]
        pp_key = account[4]

        son_object = pp_get_son(son_name)
        assert len(son_object["sidechain_public_keys"]) == 4
        for sidechain_key in son_object["sidechain_public_keys"]:
            if sidechain_key[0] == "bitcoin":
                assert sidechain_key[1] == btc_key
            elif sidechain_key[0] == "ethereum":
                assert sidechain_key[1] == ethereum_key
            elif sidechain_key[0] == "hive":
                assert sidechain_key[1] == hive_key
            elif sidechain_key[0] == "peerplays":
                assert sidechain_key[1] == pp_key
            else:
                # There should not be fifth type of key
                assert False


@pytest.mark.order(1)
def test_sidechain_addresses():
    # Check Bitcoin
    for i in range(0, len(PP_EXPECTED_REGULAR_ACCOUNTS)):

        btc_sidechain = pp_get_sidechain_address_by_account_and_sidechain(
            PP_EXPECTED_REGULAR_ACCOUNTS[i], "bitcoin")
        assert btc_sidechain["deposit_public_key"] == PP_EXPECTED_BITCOIN_SIDECHAINS[i][0]
        assert btc_sidechain["withdraw_public_key"] == PP_EXPECTED_BITCOIN_SIDECHAINS[i][1]
        assert btc_sidechain["withdraw_address"] == PP_EXPECTED_BITCOIN_SIDECHAINS[i][2]

        ethereum_sidechain = pp_get_sidechain_address_by_account_and_sidechain(
            PP_EXPECTED_REGULAR_ACCOUNTS[i], "ethereum")
        assert ethereum_sidechain["deposit_public_key"] == PP_EXPECTED_ETHEREUM_SIDECHAINS[i][0]
        assert ethereum_sidechain["deposit_address"] == PP_EXPECTED_ETHEREUM_SIDECHAINS[i][1]
        assert ethereum_sidechain["withdraw_public_key"] == PP_EXPECTED_ETHEREUM_SIDECHAINS[i][2]
        assert ethereum_sidechain["withdraw_address"] == PP_EXPECTED_ETHEREUM_SIDECHAINS[i][3]

        hive_sidechain = pp_get_sidechain_address_by_account_and_sidechain(
            PP_EXPECTED_REGULAR_ACCOUNTS[i], "hive")
        assert hive_sidechain["deposit_public_key"] == PP_EXPECTED_HIVE_SIDECHAINS[i][0]
        assert hive_sidechain["deposit_address"] == PP_EXPECTED_HIVE_SIDECHAINS[i][1]
        assert hive_sidechain["withdraw_public_key"] == PP_EXPECTED_HIVE_SIDECHAINS[i][2]
        assert hive_sidechain["withdraw_address"] == PP_EXPECTED_HIVE_SIDECHAINS[i][3]

        pp_sidechain = pp_get_sidechain_address_by_account_and_sidechain(
            PP_EXPECTED_REGULAR_ACCOUNTS[i], "peerplays")
        assert pp_sidechain["deposit_public_key"] == PP_EXPECTED_PEERPLAYS_SIDECHAINS[i][0]
        assert pp_sidechain["deposit_address"] == PP_EXPECTED_PEERPLAYS_SIDECHAINS[i][1]
        assert pp_sidechain["withdraw_public_key"] == PP_EXPECTED_PEERPLAYS_SIDECHAINS[i][2]
        assert pp_sidechain["withdraw_address"] == PP_EXPECTED_PEERPLAYS_SIDECHAINS[i][3]
