#!/usr/bin/python3

import docker
import pexpect
import requests
import re

from docker_utils import *
from json_utils import *
from string_utils import *

ETH_PRECISION = 10 ** 18
URL = "http://10.11.12.202:8545"


def eth_unlock_account(_addr):
    eth_docker_container = get_docker_container_by_image_name(
        ETH_DOCKER_IMAGE_NAME)
    geth = pexpect.spawn(
        'docker exec -it ' +
        eth_docker_container.short_id +
        " ./geth --exec 'personal.unlockAccount(\"{}\", \"\", 1200)' attach ./network/geth.ipc".format(_addr))
    time.sleep(10)
    return geth


def eth_send_command(_method, _params):
    req = {
        "jsonrpc": "2.0",
        "method": _method,
        "params": _params,
        "id": 1}
    json_obj = convert_byte_output_to_json(
        requests.post(URL, json=req).content)
    print(CMD_LOG_PREFIX + "Ethereum command:\n{} {}\n-->\n{}\n".format(_method,
                                                                        _params, json.dumps(json_obj)))
    return json_obj["result"]


def eth_hex_to_int(_s):
    return int(_s, 16)


def eth_int_to_hex(_n):
    return "0x{:X}".format(_n)


def eth_getBalance(_addr):
    return eth_hex_to_int(
        eth_send_command(
            "eth_getBalance", [
                _addr, "latest"]))


def eth_gasPrice():
    return eth_hex_to_int(eth_send_command(
        "eth_gasPrice", []))


def eth_call(_to):
    return eth_hex_to_int(eth_send_command(
        "eth_call", [{"to": _to, "data": "0x12065fe0"}, "latest"]))


def eth_sendTransaction(_from, _to, _amount):
    _hash = eth_send_command(
        "eth_sendTransaction", [
            {"from": _from, "to": _to, "value": eth_int_to_hex(_amount)}])
    return _hash


def eth_getTransactionReceipt(_hash):
    return eth_send_command("eth_getTransactionReceipt", [_hash])


def eth_waitTransaction(_hash):
    while True:
        receipt = eth_getTransactionReceipt(_hash)
        if receipt is not None:
            return eth_hex_to_int(receipt["status"]) == 1
        time.sleep(10)
