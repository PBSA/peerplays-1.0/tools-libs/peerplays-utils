#!/usr/bin/python3

import docker
import pexpect
import re

from docker_utils import *
from json_utils import *
from string_utils import *

HIVE_PRECISION = 10 ** 3

HIVE_CLI_WALLET = None


def init_hive_cli_wallet():
    hive_docker_container = get_docker_container_by_image_name(
        HIVE_DOCKER_IMAGE_NAME)
    hive_cli_wallet = pexpect.spawn(
        'docker exec -it ' +
        hive_docker_container.short_id +
        ' ./cli_wallet')
    hive_cli_wallet.expect('locked >>> ')
    hive_cli_wallet.sendline('unlock password')
    hive_cli_wallet.expect('unlocked >>> ')
    return hive_cli_wallet


def hive_cli_wallet_send_command(_command):
    global HIVE_CLI_WALLET
    if HIVE_CLI_WALLET is None:
        HIVE_CLI_WALLET = init_hive_cli_wallet()
    HIVE_CLI_WALLET.sendline(_command)
    HIVE_CLI_WALLET.expect('unlocked >>> ')
    resp = HIVE_CLI_WALLET.before.decode("utf-8")

    # Trim the beginning of the response
    split_lines = resp.splitlines()
    start = 0

    # First, skip everything before the first complete instance of command echo,
    # because it's sometimes preceded by the crippled one - having duplicated
    # characters and randomly splitted in lines
    while start < len(split_lines):
        if split_lines[start].find(_command) == 0:  # command echo
            break
        else:
            start += 1

    while start < len(split_lines):
        if (len(split_lines[start]) == 0) or (            # blank line
                split_lines[start].find(_command) == 0):  # command echo - it still can happen the second time
            start += 1
        # noise from the wallet
        elif "wallet.cpp" in split_lines[start] or "websocket_api.cpp" in split_lines[start]:
            start += 2
        elif re.match(r"\x1b.*", split_lines[start]):
            start += 1
        else:
            break
    response = '\n'.join(split_lines[start:])
    print(CMD_LOG_PREFIX + "Hive command:\n{}\n-->\n{}\n".format(_command, response))
    return convert_string_to_json(response)


def hive_about():
    return hive_cli_wallet_send_command('about')


def hive_info():
    return hive_cli_wallet_send_command('info')


def hive_suggest_brain_key():
    return hive_cli_wallet_send_command("suggest_brain_key")


def hive_create_account_with_keys(
        _creator,
        _account_name,
        _json_meta,
        _owner,
        _active,
        _posting,
        _memo):
    return hive_cli_wallet_send_command(
        'create_account_with_keys {} {} {} {} {} {} {} true'.format(
            _creator, _account_name, _json_meta, _owner, _active, _posting, _memo))


def hive_get_account(_account_name):
    return hive_cli_wallet_send_command(
        'get_account {}'.format(_account_name))


def hive_get_balance(_account_name):
    s = hive_get_account(_account_name)["balance"]
    m = re.match(r"([\d.]+) TESTS", s)
    assert m is not None
    return float(m.group(1))


def hive_get_hbd_balance(_account_name):
    s = hive_get_account(_account_name)["hbd_balance"]
    m = re.match(r"([\d.]+) TBD", s)
    assert m is not None
    return float(m.group(1))


def hive_list_accounts(_lower_bound, _limit):
    return hive_cli_wallet_send_command(
        'list_accounts "{}" {}'.format(
            _lower_bound, _limit))


def hive_transfer(_from, _to, _amount, _asset_symbol, _memo=None):
    return hive_cli_wallet_send_command(
        'transfer {} {} "{:.3f} {}" {} true'.format(
            _from,
            _to,
            _amount,
            _asset_symbol,
            "null" if _memo is None else '"{}"'.format(_memo)))


def hive_list_keys():
    return hive_cli_wallet_send_command('list_keys')
