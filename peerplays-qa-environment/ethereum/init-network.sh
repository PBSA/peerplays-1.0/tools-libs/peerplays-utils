./geth --exec 'personal.unlockAccount("0x5fbbb31be52608d2f52247e8400b7fcaa9e0bc12", "", 1200)' attach ./network/geth.ipc

curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
apt-get install -y nodejs
npm install --global yarn

git clone https://gitlab.com/PBSA/peerplays-ethereum.git
cd peerplays-ethereum
git checkout master
npm install
npx hardhat compile
npx hardhat run scripts/deploy.js --network localhost
npx hardhat run scripts/deploy_simple_token.js --network localhost
cd ..
